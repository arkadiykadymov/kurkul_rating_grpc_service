package kurkul.wh.grpc.converters;

import com.kurkul.wh.grpc.RatingGenerator;

import java.math.BigDecimal;
import java.math.BigInteger;

public class UtilConverter {

    public static BigDecimal toBigDecimal(RatingGenerator.BigDecimal rating) {
        return new BigDecimal(BigInteger.valueOf(rating.getUnscaledVal()), rating.getScale());
    }

    public static RatingGenerator.BigDecimal fromBigDecimal(BigDecimal rating) {
        return RatingGenerator.BigDecimal
                .newBuilder()
                .setUnscaledVal(rating.unscaledValue().longValue())
                .setScale(rating.scale())
                .build();
    }
}
